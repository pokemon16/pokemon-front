import Vue from 'vue'
import VueRouter from 'vue-router'
import IndexView from '../views/indexView.vue'
import Pokemon from '../components/Pokemon.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: IndexView
  },
  {
    path: '/pokemon',
    name: 'pokemon',
    component: Pokemon
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
