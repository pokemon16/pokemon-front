import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios';
Vue.config.productionTip = false
axios.defaults.baseURL= 'https://stormy-beyond-10146.herokuapp.com/api/'
import VueSpinners from 'vue-spinners';
Vue.use(VueSpinners)
import VueToastr from "vue-toastr";
// register component
Vue.component("vue-toastr", VueToastr);
new Vue({
  router,
  store,
  vuetify,
  VueSpinners,
  VueToastr,
  render: h => h(App)
}).$mount('#app')
